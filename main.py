# required libraries
import pandas as pd
from sklearn.linear_model import LinearRegression
import numpy as np
from tabulate import tabulate

"""
Data set description

# Attributes for both student-mat.csv (Math course) and student-por.csv (Portuguese language course) datasets:
1 school - student's school (binary: 'GP' - Gabriel Pereira or 'MS' - Mousinho da Silveira)
2 sex - student's sex (binary: 'F' - female or 'M' - male)
3 age - student's age (numeric: from 15 to 22)
4 address - student's home address type (binary: 'U' - urban or 'R' - rural)
5 famsize - family size (binary: 'LE3' - less or equal to 3 or 'GT3' - greater than 3)
6 Pstatus - parent's cohabitation status (binary: 'T' - living together or 'A' - apart)
7 Medu - mother's education (numeric: 0 - none, 1 - primary education (4th grade), 2 â€“ 5th to 9th grade, 3 â€“ secondary education or 4 â€“ higher education)
8 Fedu - father's education (numeric: 0 - none, 1 - primary education (4th grade), 2 â€“ 5th to 9th grade, 3 â€“ secondary education or 4 â€“ higher education)
9 Mjob - mother's job (nominal: 'teacher', 'health' care related, civil 'services' (e.g. administrative or police), 'at_home' or 'other')
10 Fjob - father's job (nominal: 'teacher', 'health' care related, civil 'services' (e.g. administrative or police), 'at_home' or 'other')
11 reason - reason to choose this school (nominal: close to 'home', school 'reputation', 'course' preference or 'other')
12 guardian - student's guardian (nominal: 'mother', 'father' or 'other')
13 traveltime - home to school travel time (numeric: 1 - <15 min., 2 - 15 to 30 min., 3 - 30 min. to 1 hour, or 4 - >1 hour)
14 studytime - weekly study time (numeric: 1 - <2 hours, 2 - 2 to 5 hours, 3 - 5 to 10 hours, or 4 - >10 hours)
15 failures - number of past class failures (numeric: n if 1<=n<3, else 4)
16 schoolsup - extra educational support (binary: yes or no)
17 famsup - family educational support (binary: yes or no)
18 paid - extra paid classes within the course subject (Math or Portuguese) (binary: yes or no)
19 activities - extra-curricular activities (binary: yes or no)
20 nursery - attended nursery school (binary: yes or no)
21 higher - wants to take higher education (binary: yes or no)
22 internet - Internet access at home (binary: yes or no)
23 romantic - with a romantic relationship (binary: yes or no)
24 famrel - quality of family relationships (numeric: from 1 - very bad to 5 - excellent)
25 freetime - free time after school (numeric: from 1 - very low to 5 - very high)
26 goout - going out with friends (numeric: from 1 - very low to 5 - very high)
27 Dalc - workday alcohol consumption (numeric: from 1 - very low to 5 - very high)
28 Walc - weekend alcohol consumption (numeric: from 1 - very low to 5 - very high)
29 health - current health status (numeric: from 1 - very bad to 5 - very good)
30 absences - number of school absences (numeric: from 0 to 93)

# these grades are related with the course subject, Math or Portuguese:
31 G1 - first period grade (numeric: from 0 to 20)
31 G2 - second period grade (numeric: from 0 to 20)
32 G3 - final grade (numeric: from 0 to 20, output target)

SOURCE: https://archive.ics.uci.edu/ml/datasets/student+performance
"""

def load_data():
    """
    loading data from csv file

    :return: data
    """
    # Read data from file 'student.csv'
    data = pd.read_csv("student.csv", sep=';')
    print("Data loaded from file")
    return data


def separate_test_train_data(data):
    """
    Separate the data to train and test set.

    :param data: the data that loaded
    :return: train, test
    """

    data['split'] = np.random.randn(data.shape[0], 1)

    # Scale of train/test
    msk = np.random.rand(len(data)) <= 0.8

    train = data[msk]
    test = data[~msk]
    del train['split']
    del test['split']
    print("Data set separated to train set and test set.")
    return train, test


def remove_non_numeric_columns(data):
    """
    Remove columns from data that type of value isn't number

    :param data: the data that loaded
    :return: the data that cleaned

    """
    return data._get_numeric_data()


def show_data(data):
    """
    Show data as a table

    :param data: data that loaded
    :return: nothing
    """
    print(tabulate(data, headers='keys', tablefmt='psql', showindex=False))
    print()


def learn_regression_model(train_x, train_y):
    """
    Learn a regression model via train data

    :param train_x: x part of train set
    :param train_y: y part of train set
    :return: regression model
    """

    # Create a model
    model = LinearRegression()

    # Fit train data to model
    model.fit(train_x, train_y)

    print("The regression model learned.")
    return model


def convert_data_to_x_y(data):
    """
    Convert a data set to array of x and array of y
    :param data: the data that processed
    :return: array of x, array of y
    """

    # Last column contains label of record
    x = np.array(data.iloc[:, :-1].values)
    y = np.array(data.iloc[:, -1].values)
    return x, y


def test_model(model, test_x, test_y):
    """
    Test a regression model with test data

    :param model: the regression model that learned.
    :param test_x: x part of test data
    :param test_y: y part of test data
    :return: nothing
    """
    # Predict via model
    test_predicted = model.predict(test_x)
    number_test = len(test_y)
    sum_error = 0
    sum_square_error = 0
    # Check error for any test data
    for i in range(number_test):
        error = abs(test_predicted[i]-test_y[i])
        sum_error += error
        sum_square_error += pow(error, 2)

    # Calculate accuracy via MAE and MSE
    mean_absolute_error = sum_error / number_test
    mean_square_error = sum_square_error / number_test
    print(f"\nAccuracy (Mean Absolute Error): {mean_absolute_error}")
    print(f"Accuracy (Mean Square Error): {mean_square_error}")


if __name__ == '__main__':
    data = load_data()
    data = remove_non_numeric_columns(data)
    train_data, test_data = separate_test_train_data(data)
    train_x, train_y = convert_data_to_x_y(train_data)
    test_x, test_y = convert_data_to_x_y(test_data)
    model = learn_regression_model(train_x, train_y)
    test_model(model, test_x, test_y)

